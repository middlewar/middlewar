MiddleWar Project
=================

A Symfony project created on September 2, 2015, 7:46 pm.


# Instruction pour l'installation #
* Récupérer le projet 

```
#!git
git clone https://BertrandD@bitbucket.org/BertrandD/middlewar.git
```

* exécuter la commande 

```
#!php 
php composer.phar install

```
* A la fin de l'installation, le script vous demandera les identifiants de connexion à la base de donnée