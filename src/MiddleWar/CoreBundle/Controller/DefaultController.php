<?php

namespace MiddleWar\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MiddleWarCoreBundle:Default:index.html.twig');
    }
}
