<?php

namespace MiddleWar\CoreBundle\Entity\Building;

use Doctrine\ORM\Mapping as ORM;

/**
 * AbstractBuilding
 *
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 */
abstract class AbstractBuilding
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="descr", type="text")
     */
    protected $descr;

    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="integer")
     */
    protected $level;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer")
     */
    protected $size;

    /**
     * @var integer
     *
     * @ORM\Column(name="maxLevel", type="integer")
     */
    protected $maxLevel;

    /**
     * @var array
     *
     * @ORM\Column(name="resourcesRequirements", type="array")
     */
    protected $resourcesRequirements;

    /**
     * @var array
     *
     * @ORM\Column(name="warBonus", type="array")
     */
    protected $warBonus;

    public abstract function display();

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AbstractBuilding
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set descr
     *
     * @param string $descr
     * @return AbstractBuilding
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string 
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return AbstractBuilding
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return AbstractBuilding
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set maxLevel
     *
     * @param integer $maxLevel
     * @return AbstractBuilding
     */
    public function setMaxLevel($maxLevel)
    {
        $this->maxLevel = $maxLevel;

        return $this;
    }

    /**
     * Get maxLevel
     *
     * @return integer 
     */
    public function getMaxLevel()
    {
        return $this->maxLevel;
    }

    /**
     * Set resourcesRequirements
     *
     * @param array $resourcesRequirements
     * @return AbstractBuilding
     */
    public function setResourcesRequirements($resourcesRequirements)
    {
        $this->resourcesRequirements = $resourcesRequirements;

        return $this;
    }

    /**
     * Get resourcesRequirements
     *
     * @return array 
     */
    public function getResourcesRequirements()
    {
        return $this->resourcesRequirements;
    }

    /**
     * Set warBonus
     *
     * @param array $warBonus
     * @return AbstractBuilding
     */
    public function setWarBonus($warBonus)
    {
        $this->warBonus = $warBonus;

        return $this;
    }

    /**
     * Get warBonus
     *
     * @return array 
     */
    public function getWarBonus()
    {
        return $this->warBonus;
    }
}
