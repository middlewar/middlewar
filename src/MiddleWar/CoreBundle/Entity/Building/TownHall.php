<?php

namespace MiddleWar\CoreBundle\Entity\Building;

use Doctrine\ORM\Mapping as ORM;

/**
 * TownHall
 *
 * @ORM\Entity
 */
class TownHall extends AbstractBuilding
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function display(){

    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
