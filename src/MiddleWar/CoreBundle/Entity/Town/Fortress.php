<?php

namespace MiddleWar\CoreBundle\Entity\Town;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fortress
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Fortress extends AbstractTown
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
