<?php

namespace MiddleWar\CoreBundle\Entity\Town;

use Doctrine\ORM\Mapping as ORM;

/**
 * AbstractTown
 *
 * @ORM\MappedSuperclass
 */
class AbstractTown
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="points", type="bigint")
     */
    protected $points;

    /**
     * @var integer
     *
     * @ORM\Column(name="x", type="integer")
     */
    protected $x;

    /**
     * @var integer
     *
     * @ORM\Column(name="y", type="integer")
     */
    protected $y;

    /**
     * @var array
     *
     * @ORM\Column(name="stock", type="array")
     */
    protected $stock;

    /**
     * @var array
     *
     * @ORM\Column(name="warBonus", type="array")
     */
    protected $warBonus;

    /**
     * @ORM\ManyToOne(targetEntity="MiddleWar\CoreBundle\Entity\Player")
     * @ORM\JoinColumn(name="owner", referencedColumnName="id")
     **/
    protected $owner;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AbstractTown
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set points
     *
     * @param integer $points
     * @return AbstractTown
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer 
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set x
     *
     * @param integer $x
     * @return AbstractTown
     */
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get x
     *
     * @return integer 
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set y
     *
     * @param integer $y
     * @return AbstractTown
     */
    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    /**
     * Get y
     *
     * @return integer 
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * Set stock
     *
     * @param array $stock
     * @return AbstractTown
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return array 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set warBonus
     *
     * @param array $warBonus
     * @return AbstractTown
     */
    public function setWarBonus($warBonus)
    {
        $this->warBonus = $warBonus;

        return $this;
    }

    /**
     * Get warBonus
     *
     * @return array 
     */
    public function getWarBonus()
    {
        return $this->warBonus;
    }

    /**
     * Set owner
     *
     * @param \MiddleWar\CoreBundle\Entity\Player $owner
     * @return AbstractTown
     */
    public function setOwner(\MiddleWar\CoreBundle\Entity\Player $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \MiddleWar\CoreBundle\Entity\Player 
     */
    public function getOwner()
    {
        return $this->owner;
    }
}
