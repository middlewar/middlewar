<?php
namespace MiddleWar\CoreBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Accueil', array('route' => 'home'));
        if (true === $this->container->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $menu->addChild('Déconnexion', array('route' => 'fos_user_security_logout'));
        }else{
            $menu->addChild('Connexion', array('route' => 'fos_user_security_login'));
            $menu->addChild('Inscription', array('route' => 'fos_user_registration_register'));
        }
    return $menu;
    }
}